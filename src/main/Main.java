package main;

public class Main {

	public static void main(String[] args) {
		// Some lines before
		System.out.println("This line comes before the greeting message bellow.");
		System.out.println("Hello world");
		
		// Some lines after the after
		System.out.println("Line 2. After the greeting");
		
		// Some lines after
		System.out.println("Line 1. After greeting");
	}

}
